'use strict';
module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define('Comment', {
    orgName: DataTypes.STRING,
    comment: DataTypes.TEXT,
    deletedAt: DataTypes.DATE
  }, {
    indexes: [
      {
        unique: false,
        fields: ['orgName']
      }
    ]
  });
  Comment.associate = function(models) {
    // associations can be defined here
  };
  return Comment;
};
