'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
      'Comments',
      [
        {
          orgName: 'xendit',
          comment: 'Looking to hire SE Asia\'s top dev talent!',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Comments', null, {}),
};
