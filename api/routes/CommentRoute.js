import { Router } from 'express';
import CommentController from '../controllers/CommentController';

const router = Router();

router.get('/:orgName/comments', CommentController.getAllByOrgName);
router.post('/:orgName/comments', CommentController.create);
router.delete('/:orgName/comments', CommentController.delete);

export default router;
