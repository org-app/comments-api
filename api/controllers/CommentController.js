import _ from 'lodash';

import CommentService from '../services/CommentService';
import ResponseHelper from '../helpers/ResponseHelper';
import {ERROR_CODES} from "../constants/ErrorResponses";

class CommentController {
  static async getAllByOrgName(req, res) {
    try {
      const comments = await CommentService.getAllByOrgName(req.params.orgName);
      ResponseHelper.jsonSuccess(res, comments);
    } catch (error) {
      ResponseHelper.error(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
    }
  }

  static async create(req, res) {
    if (!req.body || !req.body.comment || !_.isString(req.body.comment)) {
      ResponseHelper.error(res, ERROR_CODES.BAD_REQUEST);
      return;
    }

    try {
      const comment = await CommentService.create(
        req.params.orgName, _.pick(req.body, 'comment')
      );
      ResponseHelper.jsonSuccess(res, _.pick(comment, 'comment'));
    } catch (error) {
      ResponseHelper.error(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
    }
  }

  static async delete(req, res) {
    try {
      await CommentService.delete(req.params.orgName);
      ResponseHelper.noContent(res);
    } catch (error) {
      ResponseHelper.error(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
    }
  }
}

export default CommentController;
