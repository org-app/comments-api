import _ from 'lodash';
import uuid from 'uuid';

import database from '../../database/models';

class CommentService {
  static async getAllByOrgName(orgName) {
    try {
      return await database.Comment.findAll({
        where: {
          orgName: orgName,
          deletedAt: null
        },
        attributes: ['comment']
      });
    } catch (error) {
      throw error;
    }
  }

  static async create(orgName, newComment) {
    try {
      const extendedComment = _.extend({
        id: uuid.v4(), // TODO: Find a way to generate id in the model layer
        orgName: orgName
      }, newComment);
      return await database.Comment.create(extendedComment);
    } catch (error) {
      throw error;
    }
  }

  static async delete(orgName) {
    try {
      return await database.Comment.update({
        deletedAt: new Date()
      }, {
        where: {orgName: orgName}
      });
    } catch (error) {
      throw error;
    }
  }
}

export default CommentService;
