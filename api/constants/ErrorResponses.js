export const ERROR_CODES = {
  INTERNAL_SERVER_ERROR: 0,
  BAD_REQUEST: 1
};

export const ERROR_RESPONSES = {
  [ERROR_CODES.INTERNAL_SERVER_ERROR]: {
    status: 500,
    json: {
      error: {
        message: 'Something went wrong. Please try again later.'
      }
    }
  },
  [ERROR_CODES.BAD_REQUEST]: {
    status: 400,
    json: {
      error: {
        message: 'Invalid request.'
      }
    }
  }
};
