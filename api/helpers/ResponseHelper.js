import {ERROR_CODES, ERROR_RESPONSES} from '../constants/ErrorResponses';

class ResponseHelper {
  static jsonSuccess(res, json) {
    if (res && typeof res.json === 'function' && json) {
      res.json(json);
    }
  }

  static error(res, errorCode) {
    let error;
    if (errorCode in ERROR_RESPONSES) {
      error = ERROR_RESPONSES[errorCode];
    } else {
      error = ERROR_RESPONSES[ERROR_CODES.INTERNAL_SERVER_ERROR];
    }
    res.status(error.status).json(error.json);
  }

  static noContent(res) {
    res.status(204).send();
  }
}

export default ResponseHelper;
