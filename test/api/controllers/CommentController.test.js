import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);
const expect = chai.expect;

import ResponseHelper from '../../../api/helpers/ResponseHelper';
import CommentController from '../../../api/controllers/CommentController';
import CommentService from '../../../api/services/CommentService';
import {ERROR_CODES} from '../../../api/constants/ErrorResponses';

describe('CommentController', () => {
  const res = {};
  const orgName = 'xendit';

  describe('getAllByOrgName()', () => {
    const req = {
      params: {
        orgName: orgName
      },
    };
    const comments = [
      {
        comment: 'This is a sample comment.'
      }
    ];

    describe('when CommentService returns no error', () => {
      let getStub, successStub;

      beforeEach(() => {
        getStub = sinon
          .stub(CommentService, 'getAllByOrgName')
          .returns(Promise.resolve(comments));
        successStub = sinon.stub(ResponseHelper, 'jsonSuccess');
      });

      afterEach(() => {
        getStub.restore();
        successStub.restore();
      });

      it('should call ResponseHelper.success with the result', async () => {
        await CommentController.getAllByOrgName(req, res);

        expect(getStub).to.have.been.calledOnce;
        expect(getStub).to.have.been.calledWith(req.params.orgName);
        expect(successStub).to.have.been.calledOnce;
        expect(successStub).to.have.been.calledWith(res, comments);
      });
    });

    describe('when CommentService returns an error', () => {
      let getStub, errorStub;

      beforeEach(() => {
        getStub = sinon
          .stub(CommentService, 'getAllByOrgName')
          .returns(Promise.reject('error'));
        errorStub = sinon.stub(ResponseHelper, 'error');
      });

      afterEach(() => {
        getStub.restore();
        errorStub.restore();
      });

      it('should call ResponseHelper.error with INTERNAL_SERVER_ERROR', async () => {
        await CommentController.getAllByOrgName(req, res);

        expect(getStub).to.have.been.calledOnce;
        expect(getStub).to.have.been.calledWith(req.params.orgName);
        expect(errorStub).to.have.been.calledOnce;
        expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
      });
    });
  });

  describe('create()', () => {
    let createStub;

    afterEach(() => {
      createStub.restore();
    });

    describe('when input parameters are invalid', () => {
      let errorStub;

      beforeEach(() => {
        errorStub = sinon.stub(ResponseHelper, 'error');
      });

      afterEach(() => {
        errorStub.restore();
      });

      describe('when req.body is falsy', () => {
        const req = {
          params: {
            orgName: orgName
          }
        };

        beforeEach(() => {
          createStub = sinon.stub(CommentService, 'create');
        });

        it('should call ResponseHelper.error and not CommentService.create', async () => {
          await CommentController.create(req, res);

          expect(errorStub).to.have.been.calledOnce;
          expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.BAD_REQUEST);
          expect(createStub).to.not.have.been.called;
        });
      });

      describe('when req.body.comment is falsy', () => {
        const req = {
          params: {
            orgName: orgName
          },
          body: {}
        };

        it('should call ResponseHelper.error and not CommentService.create', async () => {
          await CommentController.create(req, res);

          expect(errorStub).to.have.been.calledOnce;
          expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.BAD_REQUEST);
          expect(createStub).to.not.have.been.called;
        });
      });

      describe('when req.body.comment is not a string', () => {
        const req = {
          params: {
            orgName: orgName
          },
          body: {
            comment: 1
          }
        };

        it('should call ResponseHelper.error and not CommentService.create', async () => {
          await CommentController.create(req, res);

          expect(errorStub).to.have.been.calledOnce;
          expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.BAD_REQUEST);
          expect(createStub).to.not.have.been.called;
        });
      });
    });

    describe('when parameters are valid', () => {
      const commentText = 'This is a comment.';
      const req = {
        params: {
          orgName: orgName
        },
        body: {
          comment: commentText
        }
      };

      describe('when CommentService successfully creates new comment', () => {
        let createStub, successStub;
        const dbComment = {
          id: '7c6188b6-1b1b-4275-8258-08be3dfc7043',
          orgName: orgName,
          comment: commentText
        };

        beforeEach(() => {
          createStub = sinon
            .stub(CommentService, 'create')
            .returns(Promise.resolve(dbComment));
          successStub = sinon.stub(ResponseHelper, 'jsonSuccess');
        });

        afterEach(() => {
          createStub.restore();
          successStub.restore();
        });

        it('should call ResponseHelper.success with the result', async () => {
          await CommentController.create(req, res);

          expect(createStub).to.have.been.calledOnce;
          expect(createStub).to.have.been.calledWith(orgName, {
            comment: commentText
          });
          expect(successStub).to.have.been.calledOnce;
          expect(successStub).to.have.been.calledWith(res, {
            comment: commentText
          });
        });
      });

      describe('when CommentService returns an error', () => {
        let createStub, errorStub;

        beforeEach(() => {
          createStub = sinon
            .stub(CommentService, 'create')
            .returns(Promise.reject('error'));
          errorStub = sinon.stub(ResponseHelper, 'error');
        });

        afterEach(() => {
          createStub.restore();
          errorStub.restore();
        });

        it('should call ResponseHelper.error with INTERNAL_SERVER_ERROR', async () => {
          await CommentController.create(req, res);

          expect(createStub).to.have.been.calledOnce;
          expect(createStub).to.have.been.calledWith(orgName, {
            comment: commentText
          });
          expect(errorStub).to.have.been.calledOnce;
          expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
        });
      });
    });
  });

  describe('delete()', () => {
    const req = {
      params: {
        orgName: orgName
      },
    };

    describe('when CommentService returns no error', () => {
      let deleteStub, noContentStub;

      beforeEach(() => {
        deleteStub = sinon
          .stub(CommentService, 'delete')
          .returns(Promise.resolve());
        noContentStub = sinon.stub(ResponseHelper, 'noContent');
      });

      afterEach(() => {
        deleteStub.restore();
        noContentStub.restore();
      });

      it('should call ResponseHelper.noContent', async () => {
        await CommentController.delete(req, res);

        expect(deleteStub).to.have.been.calledOnce;
        expect(deleteStub).to.have.been.calledWith(req.params.orgName);
        expect(noContentStub).to.have.been.calledOnce;
        expect(noContentStub).to.have.been.calledWith(res);
      });
    });

    describe('when CommentService returns an error', () => {
      let getStub, errorStub;

      beforeEach(() => {
        getStub = sinon
          .stub(CommentService, 'delete')
          .returns(Promise.reject('error'));
        errorStub = sinon.stub(ResponseHelper, 'error');
      });

      afterEach(() => {
        getStub.restore();
        errorStub.restore();
      });

      it('should call ResponseHelper.error with INTERNAL_SERVER_ERROR', async () => {
        await CommentController.delete(req, res);

        expect(getStub).to.have.been.calledOnce;
        expect(getStub).to.have.been.calledWith(req.params.orgName);
        expect(errorStub).to.have.been.calledOnce;
        expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
      });
    });
  });
});
