import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import uuid from 'uuid/v4';

chai.use(sinonChai);
const expect = chai.expect;

import database from '../../../database/models';
import CommentService from '../../../api/services/CommentService';

describe('CommentService', () => {
  const orgName = 'xendit';

  describe('getAllByOrgName()', () => {
    let findAllStub;
    const expectedComments = [
      {
        comment: 'This is a sample comment.'
      }
    ];

    afterEach(() => {
      findAllStub.restore();
    });

    describe('when sequelize successfully finds data', () => {
      beforeEach(() => {
        findAllStub = sinon
          .stub(database.Comment, 'findAll')
          .returns(Promise.resolve(expectedComments));
      });

      it('should return data', async () => {
        const actualComments = await CommentService.getAllByOrgName(orgName);

        expect(actualComments).to.eql(expectedComments);
        expect(findAllStub).to.have.been.calledOnce;
        expect(findAllStub).to.have.been.calledWith({
          where: {
            orgName: orgName,
            deletedAt: null
          },
          attributes: ['comment']
        });
      });
    });

    describe('when sequelize throws an error', () => {
      beforeEach(() => {
        findAllStub = sinon
          .stub(database.Comment, 'findAll')
          .returns(Promise.reject('error'));
      });

      it('should throw an error', async () => {
        let error;

        try {
          await CommentService.getAllByOrgName(orgName);
        } catch (thrownError) {
          error = thrownError;
        }

        expect(error).to.eql('error');
        expect(findAllStub).to.have.been.calledOnce;
        expect(findAllStub).to.have.been.calledWith({
          where: {
            orgName: orgName,
            deletedAt: null
          },
          attributes: ['comment']
        });
      });
    });
  });

  describe('create()', () => {
    let createStub, uuidStub;
    const expectedComment = {
      comment: 'This is a sample comment.'
    };
    const id = '7c6188b6-1b1b-4275-8258-08be3dfc7043';

    beforeEach(() => {
      uuidStub = sinon.stub(uuid, 'v4').returns(id);
    });

    afterEach(() => {
      createStub.restore();
      uuidStub.restore();
    });

    describe('when sequelize successfully creates new comment', () => {
      beforeEach(() => {
        createStub = sinon
          .stub(database.Comment, 'create')
          .returns(Promise.resolve(expectedComment));
      });

      it('should return created comment', async () => {
        const actualComment = await CommentService.create(orgName, {
          comment: expectedComment.comment
        });

        expect(actualComment).to.eql(expectedComment);
        expect(createStub).to.have.been.calledOnce;
        expect(createStub).to.have.been.calledWith({
          id: id,
          orgName: orgName,
          comment: expectedComment.comment
        });
      });
    });

    describe('when sequelize returns an error', () => {
      beforeEach(() => {
        createStub = sinon
          .stub(database.Comment, 'create')
          .returns(Promise.reject('error'));
      });

      it('should throw an error', async () => {
        let error;

        try {
          await CommentService.create(orgName, {
            comment: expectedComment.comment
          });
        } catch (thrownError) {
          error = thrownError;
        }

        expect(error).to.eql('error');
        expect(createStub).to.have.been.calledOnce;
        expect(createStub).to.have.been.calledWith({
          id: id,
          orgName: orgName,
          comment: expectedComment.comment
        });
      });
    });
  });

  describe('delete()', () => {
    let updateStub, clock, now;

    beforeEach(() => {
      now = new Date();
      clock = sinon.useFakeTimers(now.getTime());
    });

    afterEach(() => {
      updateStub.restore();
      clock.restore();
    });

    describe('when sequelize successfully updates data', () => {
      beforeEach(() => {
        updateStub = sinon
          .stub(database.Comment, 'update')
          .returns(Promise.resolve());
      });

      it('should call db method with correct params', async () => {
        await CommentService.delete(orgName);

        expect(updateStub).to.have.been.calledOnce;
        expect(updateStub).to.have.been.calledWith({
          deletedAt: now
        }, {
          where: {orgName: orgName}
        });
      });
    });

    describe('when sequelize throws an error', () => {
      beforeEach(() => {
        updateStub = sinon
          .stub(database.Comment, 'update')
          .returns(Promise.reject('error'));
      });

      it('should throw an error', async () => {
        let error;

        try {
          await CommentService.delete(orgName);
        } catch (thrownError) {
          error = thrownError;
        }

        expect(error).to.eql('error');
        expect(updateStub).to.have.been.calledOnce;
        expect(updateStub).to.have.been.calledWith({
          deletedAt: now
        }, {
          where: {orgName: orgName}
        });
      });
    });
  });
});
